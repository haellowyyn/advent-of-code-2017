# https://adventofcode.com/2017/day/24

from unittest import TestCase


def parse_components(inp):
    components = []
    for line in inp.splitlines():
        parts = line.split("/")
        components.append(tuple(int(p) for p in parts))
    return components


def build_bridges(components, bridge=None):
    if bridge is None:
        bridge = []

    end_port = bridge[-1][-1] if bridge else 0
    next_comps = [c for c in components if end_port in c]
    if not next_comps:
        return [bridge]

    bridges = []
    for comp in next_comps:
        components_cpy = components[:]
        components_cpy.remove(comp)
        if comp[0] != end_port:
            comp = comp[::-1]
        bridges += build_bridges(components_cpy, bridge + [comp])
    return bridges


def bridge_strength(bridge):
    return sum(sum(c) for c in bridge)


def bridge_length(bridge):
    return len(bridge)


def get_strongest_bridge(bridges):
    bridges = sorted(bridges, key=bridge_strength)
    return bridges[-1]


def get_longest_bridge(bridges):
    bridges = sorted(bridges, key=lambda b: (bridge_length(b), bridge_strength(b)))
    return bridges[-1]


class TestSolution(TestCase):
    INPUT = ("0/2\n"
             "2/2\n"
             "2/3\n"
             "3/4\n"
             "3/5\n"
             "0/1\n"
             "10/1\n"
             "9/10\n")

    def test_get_strongest_bridge(self):
        components = parse_components(self.INPUT)
        bridges = build_bridges(components)
        bridge = get_strongest_bridge(bridges)
        self.assertEqual(bridge, [(0, 1), (1, 10), (10, 9)])

    def test_get_longest_bridge(self):
        components = parse_components(self.INPUT)
        bridges = build_bridges(components)
        bridge = get_longest_bridge(bridges)
        self.assertEqual(bridge, [(0, 2), (2, 2), (2, 3), (3, 5)])


def main():
    with open("input.txt", "rt", encoding="utf-8") as fobj:
        inp = fobj.read()
    components = parse_components(inp)
    bridges = build_bridges(components)
    strongest_bridge = get_strongest_bridge(bridges)
    longest_bridge = get_longest_bridge(bridges)

    print("part 1:", bridge_strength(strongest_bridge))
    print("part 2:", bridge_strength(longest_bridge))


if __name__ == "__main__":
    main()
