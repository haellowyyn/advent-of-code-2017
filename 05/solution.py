# https://adventofcode.com/2017/day/5

from unittest import TestCase


def count_steps1(maze):
    pos = 0
    num_steps = 0
    while pos < len(maze):
        off = maze[pos]
        maze[pos] += 1
        pos += off
        num_steps += 1
    return num_steps


def count_steps2(maze):
    pos = 0
    num_steps = 0
    while pos < len(maze):
        off = maze[pos]
        maze[pos] += 1 if off < 3 else -1
        pos += off
        num_steps += 1
    return num_steps


class TestSolution(TestCase):
    def test_count_steps1(self):
        self.assertEqual(count_steps1([0, 3, 0, 1, -3]), 5)

    def test_count_steps2(self):
        self.assertEqual(count_steps2([0, 3, 0, 1, -3]), 10)


def main():
    with open("input.txt", "rt") as fobj:
        inp = fobj.read().strip()
    maze1 = [int(i) for i in inp.split()]
    maze2 = maze1[:]
    print("part 1:", count_steps1(maze1))
    print("part 2:", count_steps2(maze2))


if __name__ == "__main__":
    main()
