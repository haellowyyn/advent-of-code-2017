# https://adventofcode.com/2017/day/1

from unittest import TestCase


def captcha1(digits):
    length = len(digits)
    result = 0
    for i, digit in enumerate(digits):
        if digit == digits[(i + 1) % length]:
            result += int(digit)
    return result


def captcha2(digits):
    length = len(digits)
    result = 0
    for i, digit in enumerate(digits):
        if digit == digits[(i + length // 2) % length]:
            result += int(digit)
    return result


class TestSolution(TestCase):
    def test_captcha1(self):
        self.assertEqual(captcha1("1122"), 3)
        self.assertEqual(captcha1("1111"), 4)
        self.assertEqual(captcha1("1234"), 0)
        self.assertEqual(captcha1("91212129"), 9)

    def test_captcha2(self):
        self.assertEqual(captcha2("1212"), 6)
        self.assertEqual(captcha2("1221"), 0)
        self.assertEqual(captcha2("123425"), 4)
        self.assertEqual(captcha2("123123"), 12)
        self.assertEqual(captcha2("12131415"), 4)


def main():
    with open("input.txt", "rt") as fobj:
        inp = fobj.read().strip()
    print("part 1:", captcha1(inp))
    print("part 2:", captcha2(inp))


if __name__ == "__main__":
    main()
