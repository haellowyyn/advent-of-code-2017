# https://adventofcode.com/2017/day/15

from unittest import TestCase


def generator(seed, factor, check=lambda n: True):
    value = seed
    while True:
        value = (value * factor) % 2147483647
        if check(value):
            yield value


def count_pairs1(seed_a, seed_b):
    gen_a = generator(seed_a, 16807)
    gen_b = generator(seed_b, 48271)

    count = 0
    for _ in range(40000000):
        val_a, val_b = next(gen_a), next(gen_b)
        if lsb16(val_a) == lsb16(val_b):
            count += 1

    return count


def count_pairs2(seed_a, seed_b):
    gen_a = generator(seed_a, 16807, lambda n: n % 4 == 0)
    gen_b = generator(seed_b, 48271, lambda n: n % 8 == 0)

    count = 0
    for _ in range(5000000):
        val_a, val_b = next(gen_a), next(gen_b)
        if lsb16(val_a) == lsb16(val_b):
            count += 1

    return count


def lsb16(value):
    return value % (1 << 16)


class TestSolution(TestCase):
    def test_count_pairs1(self):
        self.assertEqual(count_pairs1(65, 8921), 588)

    def test_count_pairs2(self):
        self.assertEqual(count_pairs2(65, 8921), 309)


def main():
    print("part 1:", count_pairs1(512, 191))
    print("part 2:", count_pairs2(512, 191))


if __name__ == "__main__":
    main()
