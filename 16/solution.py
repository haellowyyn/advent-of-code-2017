# https://adventofcode.com/2017/day/16

from string import ascii_lowercase
from unittest import TestCase


def dance_round(progs, moves):
    for move in moves:
        dance_move(progs, move)


def dance_move(progs, move):
    move_id = move[0]
    if move_id == "s":
        # Spin
        num = int(move[1:])
        spin(progs, num)
    elif move_id == "x":
        # Exchange
        idx_a, idx_b = [int(i) for i in move[1:].split("/")]
        swap(progs, idx_a, idx_b)
    else:
        # Partner
        assert move_id == "p"
        name_a, name_b = move[1:].split("/")
        idx_a, idx_b = progs.index(name_a), progs.index(name_b)
        swap(progs, idx_a, idx_b)


def spin(progs, num):
    for _ in range(num):
        progs.insert(0, progs.pop())


def swap(progs, idx_a, idx_b):
    tmp = progs[idx_a]
    progs[idx_a] = progs[idx_b]
    progs[idx_b] = tmp


def dance_long(moves, num_progs=16):
    progs = list(ascii_lowercase[:num_progs])
    start_order = progs[:]

    period = 0
    while True:
        dance_round(progs, moves)
        period += 1
        if progs == start_order:
            break

    num_rounds = 1000000000 % period
    for _ in range(num_rounds):
        dance_round(progs, moves)

    return progs


class TestSolution(TestCase):
    def test_dance_round(self):
        moves = ["s1", "x3/4", "pe/b"]
        progs = list(ascii_lowercase[:5])
        dance_round(progs, moves)
        self.assertEqual(progs, ["b", "a", "e", "d", "c"])

    def test_dance_long(self):
        moves = ["s1", "x3/4", "pe/b"]
        progs = dance_long(moves, num_progs=5)
        self.assertEqual(progs, ["a", "b", "c", "d", "e"])


def main():
    with open("input.txt", "rt") as fobj:
        inp = fobj.read().strip()
    moves = inp.split(",")
    progs1 = list(ascii_lowercase[:16])
    dance_round(progs1, moves)
    print("part 1:", "".join(progs1))
    print("part 2:", "".join(dance_long(moves)))


if __name__ == "__main__":
    main()
