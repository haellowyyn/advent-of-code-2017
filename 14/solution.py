# https://adventofcode.com/2017/day/14

from collections import namedtuple
from unittest import TestCase


Coord = namedtuple("Coord", ["x", "y"])


class Hash:
    def __init__(self, length=256):
        self.lst = list(range(length))
        self.pos = 0
        self.skip_size = 0

    def do_round(self, inp):
        for length in inp:
            reverse_sublist(self.lst, self.pos, length)
            self.pos += length + self.skip_size
            self.skip_size += 1

    def do_hash(self, inp):
        lst = [ord(c) for c in inp]
        lst += [17, 31, 73, 47, 23]
        for _ in range(64):
            self.do_round(lst)

        dense_hash = []
        for i in range(0, len(self.lst), 16):
            val = 0
            for num in self.lst[i:i + 16]:
                val ^= num
            dense_hash.append(val)

        return dense_hash


def reverse_sublist(lst, pos, length):
    """
    Reverse the sublist of lst starting at pos with the given length.

    This also works if the sublist wraps around from the end to the
    beginning of lst, i.e., lst is treated like a circular buffer.
    """
    tmp = lst[:]
    for i in range(length):
        idx = (pos + i) % len(lst)
        rev_idx = (pos + length - 1 - i) % len(lst)
        lst[idx] = tmp[rev_idx]


def build_grid(key):
    grid = []
    for i in range(128):
        subkey = "{}-{}".format(key, i)
        byts = Hash().do_hash(subkey)
        row = list("".join("{:08b}".format(b) for b in byts))
        grid.append(row)
    return grid


def count_used(grid):
    return sum(r.count("1") for r in grid)


def gather_group(grid, coord):
    group = set()
    to_visit = {coord}
    while to_visit:
        coord = to_visit.pop()
        value = grid[int(coord.imag)][int(coord.real)]
        if value == "0" or coord in group:
            continue
        group.add(coord)

        neighbors = [coord - 1, coord + 1, coord - 1j, coord + 1j]
        for ncoord in neighbors:
            if (0 <= ncoord.real < len(grid[0])
                    and 0 <= ncoord.imag < len(grid)):
                to_visit.add(ncoord)

    return group


def find_groups(grid):
    groups = []
    for y, row in enumerate(grid):
        for x, square in enumerate(row):
            if square == "0":
                continue
            coord = complex(x, y)
            if any(coord in g for g in groups):
                continue
            new_group = gather_group(grid, coord)
            groups.append(new_group)
    return groups


def count_groups(grid):
    groups = find_groups(grid)
    return len(groups)


class TestSolution(TestCase):
    def test_count_used(self):
        grid = build_grid("flqrgnkx")
        self.assertEqual(count_used(grid), 8108)

    def test_count_groups(self):
        grid = build_grid("flqrgnkx")
        self.assertEqual(count_groups(grid), 1242)


def main():
    key = "amgozmfv"
    grid = build_grid(key)
    print("part 1:", count_used(grid))
    print("part 2:", count_groups(grid))


if __name__ == "__main__":
    main()
