# https://adventofcode.com/2017/day/19

from unittest import TestCase


class Grid:
    def __init__(self, spec):
        self.rows = spec.splitlines()

    def field(self, pos):
        x, y = int(pos.real), int(pos.imag)
        try:
            return self.rows[y][x]
        except IndexError:
            return " "


def walk(grid):
    pos = grid.rows[0].index("|")
    direction = 1j
    num_steps = 1
    word = ""

    while direction:
        pos += direction
        num_steps += 1
        char = grid.field(pos)
        if char not in "|-+":
            word += char
        direction = next_direction(grid, pos, direction)

    return word, num_steps


def next_direction(grid, pos, curr_direction):
    # Try the current direction first, then orthogonal directions.
    directions = [curr_direction, curr_direction * 1j, curr_direction * -1j]
    for direction in directions:
        pos_d = pos + direction
        if grid.field(pos_d) != " ":
            return direction

    # Nowhere left to go.
    return None


class TestSolution(TestCase):
    INPUT = ("     |          \n"
             "     |  +--+    \n"
             "     A  |  C    \n"
             " F---|----E|--+ \n"
             "     |  |  |  D \n"
             "     +B-+  +--+ \n"
             "\n")

    def test_walk(self):
        grid = Grid(self.INPUT)
        word, num_steps = walk(grid)
        self.assertEqual(word, "ABCDEF")
        self.assertEqual(num_steps, 38)


def main():
    with open("input.txt", "rt", encoding="utf-8") as fobj:
        inp = fobj.read()
    grid = Grid(inp)
    word, num_steps = walk(grid)
    print("part 1:", word)
    print("part 2:", num_steps)


if __name__ == "__main__":
    main()
