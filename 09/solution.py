# https://adventofcode.com/2017/day/9

from unittest import TestCase


class Stream:
    def __init__(self, src):
        self.src = src

    def next(self):
        while True:
            char = self.src[0]
            if char == "!":
                # Ignore this and the next char.
                self.src = self.src[2:]
                continue
            self.src = self.src[1:]
            return char

    def peek(self):
        return self.src[0]


def score_stream(stream_str):
    stream = Stream(stream_str)
    assert stream.next() == "{"
    scores = []
    parse_group(stream, scores, [])
    return sum(scores)


def count_garbage(stream_str):
    stream = Stream(stream_str)
    assert stream.next() == "{"
    garbage_counts = []
    parse_group(stream, [], garbage_counts)
    return sum(garbage_counts)


def parse_group(stream, scores, garbage_counts, current_score=1):
    scores.append(current_score)
    while True:
        c = stream.next()
        if c == "{":
            parse_group(stream, scores, garbage_counts, current_score + 1)
        elif c == "<":
            parse_garbage(stream, garbage_counts)
        elif c == "}":
            return
        else:
            assert c == ","


def parse_garbage(stream, garbage_counts):
    count = 0
    while stream.next() != ">":
        count += 1
    garbage_counts.append(count)


class TestSolution(TestCase):
    def test_score_stream(self):
        self.assertEqual(score_stream("{}"), 1)
        self.assertEqual(score_stream("{{{}}}"), 6)
        self.assertEqual(score_stream("{{},{}}"), 5)
        self.assertEqual(score_stream("{{{},{},{{}}}}"), 16)
        self.assertEqual(score_stream("{<a>,<a>,<a>,<a>}"), 1)
        self.assertEqual(score_stream("{{<ab>},{<ab>},{<ab>},{<ab>}}"), 9)
        self.assertEqual(score_stream("{{<!!>},{<!!>},{<!!>},{<!!>}}"), 9)
        self.assertEqual(score_stream("{{<a!>},{<a!>},{<a!>},{<ab>}}"), 3)

    def test_count_garbage(self):
        self.assertEqual(count_garbage("{<>}"), 0)
        self.assertEqual(count_garbage("{<random characters>}"), 17)
        self.assertEqual(count_garbage("{<<<<>}"), 3)
        self.assertEqual(count_garbage("{<{!>}>}"), 2)
        self.assertEqual(count_garbage("{<!!>}"), 0)
        self.assertEqual(count_garbage("{<!!!>>}"), 0)
        self.assertEqual(count_garbage("{<{o\"i!a,<{i<a>}"), 10)


def main():
    with open("input.txt", "rt") as fobj:
        inp = fobj.read().strip()
    print("part 1:", score_stream(inp))
    print("part 2:", count_garbage(inp))


if __name__ == "__main__":
    main()
