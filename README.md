# Advent of Code 2017 Solutions

These are my solutions of the [Advent of Code 2017](https://adventofcode.com/) coding puzzles written in Python 3.

*Note: Some of those solutions are more computation-heavy than others and benefit from being run with PyPy instead of CPython. Most scripts work with both interpreters.*

- [Day 01: Inverse Captcha](01/solution.py)
- [Day 02: Corruption Checksum](02/solution.py)
- [Day 03: Spiral Memory](03/solution.py)
- [Day 04: High-Entropy Passphrases](04/solution.py)
- [Day 05: A Maze of Twisty Trampolines, All Alike](05/solution.py)
- [Day 06: Memory Reallocation](06/solution.py)
- [Day 07: Recursive Circus](07/solution.py)
- [Day 08: I Heard You Like Registers](08/solution.py)
- [Day 09: Stream Processing](09/solution.py)
- [Day 10: Knot Hash](10/solution.py)
- [Day 11: Hex Ed](11/solution.py)
- [Day 12: Digital Plumber](12/solution.py)
- [Day 13: Packet Scanners](13/solution.py)
- [Day 14: Disk Defragmentation](14/solution.py)
- [Day 15: Dueling Generators](15/solution.py)
- [Day 16: Permutation Promenade](16/solution.py)
- [Day 17: Spinlock](17/solution.py)
- [Day 18: Duet](18/solution.py)
- [Day 19: A Series of Tubes](19/solution.py)
- [Day 20: Particle Swarm](20/solution.py)
- [Day 21: Fractal Art](21/solution.py)
- [Day 22: Sporifica Virus](22/solution.py)
- [Day 23: Coprocessor Conflagration](23/solution.py)
- [Day 24: Electromagnetic Moat](24/solution.py)
- [Day 25: The Halting Problem](25/solution.py)
